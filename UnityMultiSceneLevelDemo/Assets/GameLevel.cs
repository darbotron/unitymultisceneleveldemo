using UnityEngine;

[CreateAssetMenu( fileName = "GameLevel.asset", menuName = "Game Data/GameLevel" )]
public class GameLevel : ScriptableObject
{
	public string[] levelScenes;
	
	//----------------------------------------------------------
	public override bool Equals( object obj )
	{
		var objAsGameLevel = obj as GameLevel;

		if(	null == objAsGameLevel )
		{
			return false;
		}
		
		return Equals( objAsGameLevel );
	}

	//----------------------------------------------------------
	public bool Equals( GameLevel other )
	{
		if(	levelScenes.Length != other.levelScenes.Length )
		{
			return false;		
		}

		for( int i = 0; i < levelScenes.Length; ++i )
		{
			if( levelScenes[ i ] != other.levelScenes[ i ] )
			{
				return false;
			}
		}
		
		return true;
	}

	//----------------------------------------------------------
	public override int GetHashCode()
	{
		int hash = 0;
		for( int i = 0; i < levelScenes.Length; ++i )
		{
			hash ^= levelScenes[ i ].GetHashCode();			
		}
		return hash;
	}

	//----------------------------------------------------------
	public static bool operator==( GameLevel lhs, GameLevel rhs )
	{
		if( lhs is null ) // note: this explicitly ref-tests vs. null with no operator== or !=  
		{
			return( rhs is null );
		}

		return lhs.Equals( rhs );
	}

	//----------------------------------------------------------
	public static bool operator!=( GameLevel lhs, GameLevel rhs )
	{
		if( lhs is null )
		{
			return( ! ( rhs is null ) );
		}

		return ( ! lhs.Equals( rhs ) );
	}

}
