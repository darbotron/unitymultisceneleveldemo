using UnityEngine;

public class GameLevelObject : MonoBehaviour
{
	//----------------------------------------------------------
	void Awake()
	{
		GameManager.RegisterGameLevelObject( this );
		
		// logging to show when called in multiscene init
		Debug.Log( $"[{Time.frameCount}]{gameObject.scene.name}.{name}:MonoBehaviour.Awake" );
	}

	//----------------------------------------------------------
	void Start()
	{
		// logging to show when called in multiscene init
		Debug.Log( $"[{Time.frameCount}]{name}:MonoBehaviour.Start" );
	}

	//----------------------------------------------------------
	void Update()
	{
		++m_updateCount;
		
		if( GameManager.GameState.LevelRunning != GameManager.Instance.CurrentGameState )
		{
			// logging to show when called in multiscene init
			Debug.Log( $"[{Time.frameCount}]{gameObject.scene.name}.{name}:MonoBehaviour.Update (times updated: {m_updateCount})" );
		}
	}
	
	//----------------------------------------------------------
	void OnDestroy()
	{
		GameManager.UnRegisterGameLevelObject( this );
	}
	
	//----------------------------------------------------------
	// logging to show when called in multiscene init
	public void GameLevelInit()   => Debug.Log( $"[{Time.frameCount}]{gameObject.scene.name}.{name}.{System.Reflection.MethodBase.GetCurrentMethod()}" );
	
	//----------------------------------------------------------
	public void GameLevelUpdate()
	{
		if(	! m_loggedGameLevelUpdate )
		{
			m_loggedGameLevelUpdate = true;

			// logging to show when called in multiscene init
			Debug.Log( $"[{Time.frameCount}]{gameObject.scene.name}.{name}.{System.Reflection.MethodBase.GetCurrentMethod()}" );
		}
	}
	
	//----------------------------------------------------------
	// logging to show when called in multiscene init
	public void OnGameLevelUnloading() => Debug.Log( $"[{Time.frameCount}]{gameObject.scene.name}.{name}.{System.Reflection.MethodBase.GetCurrentMethod()}" );
	
	int  m_updateCount           = 0;
	bool m_loggedGameLevelUpdate = false;
}
