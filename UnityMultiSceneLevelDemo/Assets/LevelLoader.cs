using UnityEngine;

// this triggers the initial GameLevel load
public class LevelLoader : MonoBehaviour
{
    [SerializeField] GameLevel m_gameLevelToLoad;
	[SerializeField] bool      m_forceUnloadBeforeLoad	= true;
	[SerializeField] float     m_delayBeforeLoading		= 0f;
	
	//----------------------------------------------------------
    void Start()
	{
		StartCoroutine( CoProcessGameLevelLoad() );
	}

	//----------------------------------------------------------
	private System.Collections.IEnumerator CoProcessGameLevelLoad()
	{
		if( m_delayBeforeLoading > 0f )
		{
			yield return new WaitForSeconds( m_delayBeforeLoading );
		}

		if( m_forceUnloadBeforeLoad )
		{
			// this forces the unload of all loaded scenes, then triggers the game level to load
			GameManager.Instance.ForceUnloadAllScenes( () => GameManager.Instance.LoadGameLevel( m_gameLevelToLoad ) );		
		}
		else
		{
			GameManager.Instance.LoadGameLevel( m_gameLevelToLoad );
		}
	}
}
