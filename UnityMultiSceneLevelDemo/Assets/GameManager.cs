using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{   
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region monobehaviour
    
    //----------------------------------------------------------
    void Awake()
    {
        OnAwake_InitSingleton();
    }

    //----------------------------------------------------------
    void Update()
    {
        OnUpdate_GameLevelObjectsUpdate();
    }


    #endregion monobehaviour
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region singleton

    
    /// <summary>
    /// singletons are bad m'kay? done this way for convenience because simple sample code.
    /// please enjoy the singleton pattern responsibly.
    /// My rules for this NEVER:
    /// * lazy initialise a singleton
    /// * write singletons as static classes
    ///
    /// NOTE: Instance is assigned in Awake() 
    /// </summary>
    public static GameManager Instance { get; private set; } = null;
    
    //----------------------------------------------------------
    private void OnAwake_InitSingleton()
    {
        Debug.Assert( null == Instance, "can't have more than one of these!!" );

        Instance = this;
        DontDestroyOnLoad( gameObject );
    }

    
    #endregion singleton
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region GameLevel management
    
    
    /// used by the worker coroutine(s) to keep track of overall system state
    public enum GameState
    {
        Boot,                   /// initial state
        
        /// loading an empty scene is a simple way to force all stuff other than "DontDestroyOnLoad" objects to be unloaded
        UnloadingAllScenes,             
        EmptyScene,             /// once the empty scene is loaded this state reflects that 
                                
        /// 'normal' flow         
        LevelScenesUnloading,   /// to unload a GameLevel we unload its scenes in reverse order
        GameLevelUnloaded,    /// set ince current GameLevel scenes all unloaded 
        LevelScenesLoading,     /// loading a GameLevel: kick off async loads for scenes in the GameLevel (in order) preventing any from activating until all have loaded
        LevelScenesActivating,  /// once all are loaded we enable activation, again in order
        LevelInitialising,      /// once all loaded scenes have activated (i.e. data initialised, Awake(),Start(), called for all monobehviours in that scene (i.e. levelASyncLoadOperation.isDone == true)) we init all GameLevelObjects across all scenes in the GameLevel
        LevelRunning            /// once all GameLevelObjects are intialised we update all GameLevelObjects on GameManager.Update() 
    };

    public GameState CurrentGameState { get; private set; } = GameState.Boot;
    public GameLevel CurrentGameLevel { get; private set; } = null;
    
    public void ForceUnloadAllScenes( System.Action cbOnAllScenesUnloaded ) => StartCoroutine( CoUnloadAllScenes( cbOnAllScenesUnloaded) ); 
    public void LoadGameLevel( GameLevel levelToLoad )                      => StartCoroutine( CoLoadGameLevel( levelToLoad ) ); 
    public void UnloadCurrentGameLevel()                                    => StartCoroutine( CoUnloadCurrentGameLevel() ); 
    
    //----------------------------------------------------------
    private IEnumerator CoUnloadAllScenes( System.Action cbOnAllScenesUnloaded )
    {
        switch( CurrentGameState )
        {
        case GameState.Boot:
        case GameState.LevelRunning:
        case GameState.EmptyScene:
            break;
        
        default:
            Debug.Assert( false, $"can't trigger unload all scenes when in state {CurrentGameState}" );
            yield break;
        }
        
        CurrentGameState = GameState.UnloadingAllScenes;        
        yield return SceneManager.LoadSceneAsync( "EmptyScene", LoadSceneMode.Single );
        CurrentGameState = GameState.EmptyScene;
        
        cbOnAllScenesUnloaded?.Invoke();
    }    

    //----------------------------------------------------------
    private IEnumerator CoUnloadCurrentGameLevel()
    {
        switch( CurrentGameState )
        {
        case GameState.Boot:
        case GameState.EmptyScene:
        case GameState.LevelRunning:
            break;
        
        default:
            Debug.Assert( false, $"can't trigger a level UNload when in state {CurrentGameState}" );
            yield break;
        }

        CurrentGameState = GameState.LevelScenesUnloading;
        
        OnGameLevelBeforeUnloadScenes_GameLevelObjectsOnLevelUnloading();        
        
        var levelUnloadAsyncOps = new List< AsyncOperation >();
                        
        Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: about to start load operations" );

        // unload in opposite order to loading, because philosophically correct :D            
        for( int iNextSceneIndex = ( CurrentGameLevel.levelScenes.Length -1 ); iNextSceneIndex >= 0 ; --iNextSceneIndex )
        {                                
            var sceneName   = CurrentGameLevel.levelScenes[ iNextSceneIndex ];
            var sceneLoadOp = SceneManager.UnloadSceneAsync( sceneName );
            levelUnloadAsyncOps.Add( sceneLoadOp );
        }

        Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: waiting for all load operations to be loaded" );
            
        // n.b. I often use a { ... } scope without a control-flow keyword to confine locals to specific blocks of code 
        {
            var allDone = false;
                
            do
            {
                yield return null;
                    
                allDone = true;

                foreach( var sceneLoadOp in levelUnloadAsyncOps )
                {
                    allDone &= sceneLoadOp.isDone;                    
                }
            } 
            while( ! allDone );
        }

        CurrentGameLevel = null;        
        CurrentGameState = GameState.GameLevelUnloaded;
    }

    //----------------------------------------------------------
    private IEnumerator CoLoadGameLevel( GameLevel gameLevelToLoad )
    {
        switch( CurrentGameState )
        {
        case GameState.Boot:
        case GameState.EmptyScene:
        case GameState.LevelRunning:
            break;
        
        default:
            Debug.Assert( false, $"can't trigger a level load when in state {CurrentGameState}" );
            yield break;
        }

        // unload any loaded level
        if( null != CurrentGameLevel )
        {
            yield return CoUnloadCurrentGameLevel();            
            Debug.Assert( ( GameState.GameLevelUnloaded == CurrentGameState ), $"current state is {CurrentGameState}!! expected {GameState.GameLevelUnloaded}" );
        }
        
        // load and init the specified level
        {
            //
            // NOTE:
            // Unity Scenes async loading with allowSceneActivation = false
            // NEVER go beyond progress = 0.9 until allowSceneActivation set to true
            //
            const float k_fSceneIsLoadedProgressThreshold = 0.9f;

            CurrentGameLevel = gameLevelToLoad;
            CurrentGameState = GameState.LevelScenesLoading;
            var levelLoadAsyncOps = new List< AsyncOperation >();
                        
            Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: about to start load operations" );

            
            for( int iNextSceneIndex = 0; iNextSceneIndex <  gameLevelToLoad.levelScenes.Length; ++iNextSceneIndex )
            {                                
                var sceneName   = gameLevelToLoad.levelScenes[ iNextSceneIndex ];
                var sceneLoadOp = SceneManager.LoadSceneAsync( sceneName, LoadSceneMode.Additive );
                sceneLoadOp.allowSceneActivation = false;
                levelLoadAsyncOps.Add( sceneLoadOp );
            }

            Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: waiting for all load operations to be loaded" );
            
            {
                var allLoaded = false;
                
                do
                {
                    yield return null;

                    allLoaded = true;

                    foreach( var sceneLoadOp in levelLoadAsyncOps )
                    {
                        allLoaded &= ( ! ( sceneLoadOp.progress < k_fSceneIsLoadedProgressThreshold ) );
                    }

                } 
                while( ! allLoaded );
            }

            CurrentGameState = GameState.LevelScenesActivating;
                    
            Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: allowing scene activation" );

            foreach( var sceneLoadOp in levelLoadAsyncOps )
            {
                sceneLoadOp.allowSceneActivation = true;                
            }

            Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: waiting for all scenes to activate" );
            
            {
                var allDone = false;
                
                do
                {
                    yield return null;
                    
                    allDone = true;

                    foreach( var sceneLoadOp in levelLoadAsyncOps )
                    {
                        allDone &= sceneLoadOp.isDone;                    
                    }
                } 
                while( ! allDone );
            }
        }
    
        Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: all scenes report (.isDone == true) initialising" );
        
        CurrentGameState = GameState.LevelInitialising;

        OnGameLevelAllScenesActivated_GameLevelObjectsInitialise();
        
        Debug.Log( $"[{Time.frameCount}]{GetType().Name}.CoLoadScenes: done" );        
        CurrentGameState = GameState.LevelRunning;
    }


    #endregion GameLevel management
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #region GameLevelObject handling
    
    
    List< GameLevelObject > m_allObjects = new List< GameLevelObject >();

    //----------------------------------------------------------
    public static void RegisterGameLevelObject( GameLevelObject toRegister )
    {
        Debug.Assert( null != Instance, "can't call this without a GameManager" );
        
        if( ! ( Instance?.m_allObjects.Contains( toRegister ) ?? false ) )
        {
            Instance.m_allObjects.Add( toRegister );
        }
    }

    //----------------------------------------------------------
    public static void UnRegisterGameLevelObject( GameLevelObject toUnRegister )
    {
        Debug.Assert( null != Instance, "can't call this without a GameManager" );
        
        if( Instance?.m_allObjects.Contains( toUnRegister ) ?? false )
        {
            Instance.m_allObjects.Remove( toUnRegister );
        }
    }

    //----------------------------------------------------------
    private void OnGameLevelAllScenesActivated_GameLevelObjectsInitialise()
    {
        foreach( var gameLevelObject in m_allObjects )
        {
            gameLevelObject.GameLevelInit();
        }
    }

    //----------------------------------------------------------
    private void OnUpdate_GameLevelObjectsUpdate()
    {
        if( GameState.LevelRunning == CurrentGameState )
        {
            for( int i = 0; i < m_allObjects.Count; ++i )
            {
                m_allObjects[ i ].GameLevelUpdate();
            }
        }
    }

    //----------------------------------------------------------
    private void OnGameLevelBeforeUnloadScenes_GameLevelObjectsOnLevelUnloading()
    {
        // opposite order of calling GameLevelInit, because philosophically correct :D
        for( int i = ( m_allObjects.Count - 1 ); i >=0; --i )
        {
            m_allObjects[ i ].OnGameLevelUnloading();
        }
    }

    
    #endregion GameLevelObject handling
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
