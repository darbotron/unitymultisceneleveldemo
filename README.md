# purpose of this simple sample
Lots (most? all?) real world Unity projects use multiple additively loaded scenes.

Since simultaneous changes made to Unity scene files are not easily merged in Source Code Control using multiple additive scenes offers many organisational advantages .

In the days before nested prefabs this was basically the only way to modularly manage & reuse medium / large sets of data.

Despite working with multiple additively laoded scenes being a defacto standard, Unity currently provides no way of synchronising init / update of objects across scenes.

This super simple sample is a basic proof of concept showing how this can be managed:

- `GameLevel` represents a group of associated Unity scenes which, additively loaded, contain the objects for a logical 'level' of a game
- `GameLevelObject` represents objects which are part of the `GameLevel` & which should be initialised when all `GameLevel` scenes are loaded and Unity init has completed for all of them
- `GameManger` is responsible for managing loading / unloading of scenes in `GameLevel` objects and for initialising / updating `GameLevelObject`

# running this sample

- Clone the repo
- Open the project
- Open BootScene.unity
- press play

	- BootScene loads GameLevel00 (contains 3 scenes)
	- load is triggered by GameObject 'LevelLoader' - unloads all loaded scenes (i.e. BootScene) before loading GameLevel00
	- last scene in GameLevel00 contains another LevelLoader instance which triggers load of GameLevel01
	- loading GameLevel01 causes all scenes in GAmeLevel00 to be unloaded first
	
- the console log shows when the key parts of the process happen (inc. various standard Unity even functions)
- have a look at the code it should be fairly self explanatory :D

# license
Copyright 2022 Alex Darby - MIT licensed: https://opensource.org/licenses/MIT
- license is at LevelLoadTest/License.txt